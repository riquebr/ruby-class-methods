united_kingdom = [
  {
    name: "Scotland",
    population: 5295000,
    capital: "Edinburgh"
  },
  {
    name: "Wales",
    population: 3063000,
    capital: "Swansea"
  },
  {
    name: "England",
    population: 53010000,
    capital: "London"
  }
]

# 1. Change the capital of Wales from `"Swansea"` to `"Cardiff"`.
united_kingdom[1][:capital] = 'Cardiff'
p united_kingdom[1][:capital]
# 2. Create a Hash for Northern Ireland and add it to the `united_kingdom` array
# (The capital is Belfast, and the population is 1,811,000).
also_uk = [
  {
    name: 'Northern Ireland',
    population: 1811000,
    capital: 'Belfast',
  }
]

ultimate_uk = united_kingdom + also_uk
p ultimate_uk
# 3. Use a loop to print the names of all the countries in the UK.
for countries in ultimate_uk
  p countries[:capital]
end

# 4. Use a loop to find the total population of the UK.
total_population = 0

for folk in ultimate_uk
  total_population += folk[:population]
end

p total_population
